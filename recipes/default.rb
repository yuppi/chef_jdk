#
# Cookbook Name:: jdk
# Recipe:: default
#
# Copyright 2014, Suzuki-Yuki
#
# All rights reserved - Do Not Redistribute
#

#--------------------------------------------------------------------
# JDKのインストール
#--------------------------------------------------------------------
yum_package "java-1.8.0-openjdk" do
  action :install
end
yum_package "java-1.8.0-openjdk-devel" do
  action :install
end